const express = require('express')
const cors = require('cors');
const db = require('./queries')
const app = express()
const PORT = 5000

app.use(express.json());

console.log('Starting backend');

app.use(cors());

// MAIN ROUTE
app.get('/', (request, response) => {
  response.json({ info: 'Node.js, Express, and Postgres API' })
})

// ROUTES
app.get('/notes', db.getNotes)
app.get('/notes/:id', db.getNote)
app.post('/notes', db.createNote)
app.put('/notes/:id', db.updateNote)
app.delete('/notes/:id', db.deleteNote)


app.listen(PORT, () => {
  console.log(`App running on port ${PORT}.`)
})


