const {Pool, Client} = require('pg')

const pool = new Pool({
  user: 'me',
  host: 'localhost',
  database: 'notesapi',
  password: 'password',
  port: 5432,
})

pool.query(
  "CREATE TABLE notes (id SERIAL PRIMARY KEY, title VARCHAR(20), description VARCHAR(50), type VARCHAR(20), password TEXT)",
    (err, res) => {
      console.log(err, res)
      pool.end()
    }
)

