const Pool = require('pg').Pool

const pool = new Pool({
  user: 'me',
  host: 'localhost',
  database: 'notesapi',
  password: 'password',
  port: 5432,
})

const getNotes = async (req, res) => {
  try {
    const allNotes = await pool.query("SELECT * FROM notes ORDER BY id DESC");
    res.json(allNotes.rows)
  } catch (err) {
    console.log(err.message);
  }
}

const getNote = async (req, res) => {
  const {id } = req.params;
  try {
    const note = await pool.query("SELECT * FROM notes WHERE id = $1", [id])
    // const noteIsPrivate = note.rows[0].private
    // if (noteIsPrivate) {
    //   res.json({
    //     msg: "Use password to open the message."
    //   })    
    // } else {
    res.json(note.rows[0])  
    // }
    
  } catch (err) {
    console.log(err.message);
  }
}

const createNote = async (req, res) => {
  const {id, title, description, type, password} = req.body;
  try {
    const newNote = await pool.query("INSERT INTO notes (title, description, type, password) VALUES ($1, $2, $3, $4) RETURNING *", [title, description, type, password])
    console.table(newNote.rows);
  } catch (err) {
    console.log(err.message);
  }
}


const updateNote = async (req, res) => {
  const {id} = req.params;
  const {title, description, type } = req.body;
  try {
    const updateNote = await pool.query("update notes SET title = $1, description = $2, type = $3 WHERE id = $4", [title, description, type, id])
    res.json("Note was updated")
    console.table(req.body);
  } catch (err) {
    console.log(err.message);
  }
}

const deleteNote = async (req, res) => {
  
  try {
    const {id} = req.params;
    const deleteNote = await pool.query('DELETE FROM notes WHERE id = $1', [id])
    res.json(`Note with  was successfuly deleted`)
    console.log(`Note with ${id} was successfuly deleted`);
    
  } catch (err) {
    console.log(err.message);
  }
}

module.exports = {
  getNotes,
  createNote,
  getNote,
  updateNote,
  deleteNote,
}
